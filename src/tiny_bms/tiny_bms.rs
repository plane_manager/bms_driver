use std::{thread, time};
use rpi_embedded::uart::{Error, Parity, Queue, Uart};
use crate::tiny_bms::crc;
use crate::tiny_bms::rw_error::RWError;
use crate::tiny_bms::rw_error::RWError::*;

pub struct TinyBMS {
    uart: Uart
}

impl TinyBMS {
    pub fn new() -> Result<Self, Error> {
        let uart = Uart::with_path("/dev/serial0", 115200, Parity::None, 8, 1)?;

        Ok(TinyBMS {
            uart
        })
    }

    fn write(&mut self, msg: &[u8]) -> Result<usize, RWError> {
        let crc = crc::crc16_2(&msg);

        let len = msg.len() + 2;
        let mut to_send: Vec<u8> = Vec::new();
        to_send.reserve(len);
        to_send.resize(msg.len(), 0);
        to_send.copy_from_slice(msg);
        to_send.push(crc.lsb);
        to_send.push(crc.msb);

        log::debug!("[Tiny_BMS][UART] Writing: {:02x?}", to_send);
        let mut sent = 0;
        self.uart.flush(Queue::Both)?;
        sent += self.uart.write_bytes(&*to_send)?;
        self.uart.drain()?;
        log::debug!("[Tiny_BMS][UART] Wrote: {} bytes", sent);

        Ok(sent)
    }

    fn read(&mut self, expected_len: u8) -> Result<Vec<u8>, RWError> {
        let mut buff = Vec::new();
        buff.resize(expected_len as usize, 0);

        let mut recv = 0;
        for _ in 1..3 {
            recv += self.uart.read_bytes(&mut buff[recv..])?;
            if recv >= expected_len as usize {
                break;
            }
            log::warn!("Didnt receive expected response size on UART, waiting...");
            thread::sleep(time::Duration::from_millis(50));
        };
        log::debug!("[Tiny_BMS][UART] Read: {} bytes", recv);
        log::debug!("[Tiny_BMS][UART] Read: {:02x?}", buff);

        if recv == 0 {
            return Err(NoResponse);
        }
        if recv > expected_len as usize {
            return Err(BufferExceeded(recv));
        }
        if recv < 3 {
            return Err(ChecksumError);
        }
        let crc = crc::crc16_2(&buff[..(recv - 2)]);
        if crc.lsb != buff[recv - 2] || crc.msb != buff[recv - 1] {
            return Err(ChecksumError);
        }
        if recv == 6 && buff[0] == 0xAA
            && buff[1] == 0x00 {
            return Err(BMSError(buff[3]))
        }
        if recv < expected_len as usize {
            return Err(BadLength(buff));
        }

        buff.truncate(buff.len() - 2);
        Ok(buff)
    }

    pub fn send(&mut self, msg: &[u8], exp_resp_len: u8) -> Result<Vec<u8>, RWError> {
        self.write(&msg)?;
        self.read(exp_resp_len)
    }

    pub fn read_registers(&mut self, addr: u16, len: u8) -> Result<Vec<u8>, RWError>{
        // TinyBMS documentation 1.1.6
        let mut msg: [u8; 6] = [0; 6];
        msg[0] = 0xAA;
        msg[1] = 0x03;
        msg[2] = (addr / 256) as u8;
        msg[3] = (addr % 256) as u8;
        msg[4] = 0;
        msg[5] = len;

        let expected_response_len = (len * 2) + 5;
        let mut resp = self.send(&msg, expected_response_len)?;
        if resp[0] != 0xAA || resp[1] != 0x03 || resp[2] != len * 2 {
            return Err(RWError::CommunicationError(resp));
        }
        resp.drain(0..3);
        Ok(resp)
    }
}