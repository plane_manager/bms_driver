use std::ops::Range;
use crate::tiny_bms::commands::CommandError::HelloError;
use crate::tiny_bms::rw_error::RWError;
use crate::TinyBMS;

#[derive(Debug)]
pub enum CommandError {
    RWError(RWError),

    WrongArgument(),

    HelloError(Vec<u8>)
}

impl From<RWError> for CommandError {
    fn from(error: RWError) -> Self {
        CommandError::RWError(error)
    }
}

impl TinyBMS {
    pub fn hello_bms(&mut self) -> Result<(), CommandError> {
        let resp = self.read_registers(500, 1)?;
        log::debug!("Hello BMS! {:02x?}", resp);
        if resp[0] != 0x01 || resp[1] != 0x02 {
            return Err(HelloError(resp));
        }
        Ok(())
    }

    pub fn get_voltages(&mut self, r: Range<u8>) -> Result<Vec<u16>, CommandError> {
        if  r.end > 16 {
            return Err(CommandError::WrongArgument());
        }

        let resp = self.read_registers(r.start as u16, r.end - r.start)?;
        let mut voltages: Vec<u16> = Vec::new();
        for i in 0..resp.len()/2 {
            let j = i * 2;
            let t: u16 = resp[j + 1] as u16 + (resp[j] as u16) * 256;
            voltages.push(t);
        }
        Ok(voltages)
    }
}