use rpi_embedded::uart::Error;
use crate::tiny_bms::rw_error::RWError::UARTError;

#[derive(Debug)]
pub enum RWError {
    UARTError(Error),
    NoResponse,
    BufferExceeded(usize),
    ChecksumError,
    BadLength(Vec<u8>),

    CommunicationError(Vec<u8>),
    BMSError(u8)
}

impl From<rpi_embedded::uart::Error> for RWError {
    fn from(error: Error) -> Self {
        UARTError(error)
    }
}