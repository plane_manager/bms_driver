pub struct Crc {
    crc: u16,
    pub lsb: u8,
    pub msb: u8
}

impl Crc {
    fn new(crc: u16) -> Self {
        Crc {
            crc,
            lsb: (crc % 256) as u8,
            msb: (crc / 256) as u8
        }
    }
}

impl Into<u16> for Crc {
    fn into(self) -> u16 {
        self.crc
    }
}

impl From<u16> for Crc {
    fn from(crc: u16) -> Self {
        Crc::new(crc)
    }
}


pub fn crc16_2(msg: &[u8]) -> Crc {
    let mut crc: u16 = 0xFFFF;

    for pos in 0..msg.len() {
        crc ^= msg[pos] as u16;
        for _ in 0..8 {
            if (crc & 0x0001) != 0 {
                crc >>= 1;
                crc ^= 0xA001;
            }
            else {
                crc >>= 1;
            }
        }
    };
    Crc::new(crc)
}