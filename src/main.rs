mod tiny_bms;

use std::thread::sleep;
use std::time::Duration;
use env_logger::Env;
use crate::tiny_bms::tiny_bms::TinyBMS;

fn main() {
    println!("Hello, world!");
    env_logger::Builder::from_env(Env::default().default_filter_or("debug")).init();

    let mut bms = TinyBMS::new().expect("Cant initialize a bms!");

    loop {
        sleep(Duration::from_secs(3));

        let _ = bms.hello_bms().map_err(|e| log::error!("No hello? {:?}", e));
        let voltages = bms.get_voltages(12..16)
            .unwrap_or_else(|e| {log::error!("No voltages {:?}", e); Vec::new()});

        println!("Voltages: {:?}", voltages);

    }
}

